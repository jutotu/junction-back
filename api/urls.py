from django.conf.urls import include, url
from django.contrib import admin

from .views import UserList
from .views import UserDetail
from .views import TransactionList
from .views import UserPaymentDetail
from .views import TransactionDetail
from .views import Optimize

urlpatterns = [
    url(r'^users/$', UserList.as_view()),
    url(r'^users/([0-9]+)/$', UserDetail.as_view()),
    url(r'^users/([0-9]+)/saldo$', UserPaymentDetail.as_view()),
    url(r'^transactions/$', TransactionList.as_view()),
    url(r'^transactions/([0-9]+)/$', TransactionDetail.as_view()),
    url(r'^optimizer/', Optimize.as_view()),
]
