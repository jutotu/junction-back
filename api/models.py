from django.db import models
from django.core.files import File
from rest_framework import serializers
from django.contrib.auth.models import User

# Create your models here.

# class TransactioTag(models.Model):
CUR_EUR = 'EUR'
CUR_YEN = 'YEN'
CUR_USD = 'USD'

CURRENCIES_CHOICES = (
    (CUR_EUR, 'Euro'),
    (CUR_YEN, 'Yen'),
    (CUR_YEN, 'US Dollas'),
)


class Tag(models.Model):
    title = models.CharField(max_length=16, primary_key=True)

    def __unicode__(self):
        return self.title


class Transaction(models.Model):
    payer = models.ForeignKey(User, related_name='payments_out')
    receiver = models.ForeignKey(User, related_name='payments_in')
    amount = models.IntegerField()
    tag = models.ForeignKey(Tag, related_name='tagged')
    currency = models.CharField(max_length=3,
                                choices=CURRENCIES_CHOICES,
                                default=CUR_EUR)
    timestamp = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.payer.username + " => " + \
               self.receiver.username + ", " + str(self.amount)
