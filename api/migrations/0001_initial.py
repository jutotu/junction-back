# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('title', models.CharField(max_length=16, serialize=False, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.IntegerField()),
                ('currency', models.CharField(default=b'EUR', max_length=3, choices=[(b'EUR', b'Euro'), (b'YEN', b'Yen'), (b'YEN', b'US Dollas')])),
                ('timestamp', models.DateTimeField(auto_now=True)),
                ('payer', models.ForeignKey(related_name='payments_out', to=settings.AUTH_USER_MODEL)),
                ('receiver', models.ForeignKey(related_name='payments_in', to=settings.AUTH_USER_MODEL)),
                ('tag', models.ForeignKey(related_name='tagged', to='api.Tag')),
            ],
        ),
    ]
