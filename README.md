* GET /api/user/
  * Kaikki Userit (U?)

* GET /api/user/1
  * U1 details

* GET /api/user/1/saldo
  * U1 saldot kaikkien kanssa, joden kanssa on transaktioita
  * { '2': 50 }, U2 on velkaa 50
  * { '4': *100 }, U2:lle velkaa 100

* GET /api/transactions
  * Kaikki transactionit ? -> ?

* GET /api/transactions?receiver=2
  * Kaikki transactionit ? -> U2

* GET /api/transactions?payer=2
  * Kaikki transactionit U2 -> ?

* GET /api/transactions?payer=2&receiver=1
  * Kaikki transactionit U2 -> U1

* POST /api/user/
  * Adds user
  * call   { "username": "asdasd"}
  * return {"id": 8, "username": "asdasd"}

* POST /api/transactions/
  * Adds transaction
  * call  { "payer": 1, "receiver": 2, "amount": 12 }, (U1 -> U2, 15 rahaa)
  * return { "payer": 2, "receiver": 1, "amount": 23,
    "timestamp": "2015*11*07T12:35:18.586181Z", "currency": "EUR" }
