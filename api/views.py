from django.shortcuts import render
from django.http import Http404
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404

from rest_framework import status
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.renderers import JSONRenderer

from .serializers import UserSerializer
from .serializers import TransactionSerializer
from .filters import TransactionFilter
from .models import Transaction
# Create your views here.


class UserList(generics.ListCreateAPIView):
    model = User
    serializer_class = UserSerializer
    queryset = User.objects.all()


class Optimize(APIView):
    def get(self, request, format=None):
        params = dict(self.request.query_params.iterlists())
        print(params)
        pooldata = self.getPoolData(
            list(map((lambda x: int(x)), params['user'])))

        result = self.solvePool(pooldata)

        queryset = User.objects.all()
        serializer = UserSerializer(queryset, many=True)
        return Response(serializer.data)

    def solvePool(self, pool):
        smallest = min(pool, key=pool.get)
        print(pool)
        print(smallest)
        print(pool[smallest])

    def getPoolData(self, users):
        totalData = {}
        for u in users:
            payOut = Transaction.objects.filter(payer_id=u)  # Payer id pk
            payIn = Transaction.objects.filter(receiver_id=u)  # Payer id pk
            paymentData = {}
            for p in payOut:
                paymentData[p.receiver.id] = 0
                paymentData[p.receiver.id] = 0

            for p in payIn:
                paymentData[p.payer.id] = 0
                paymentData[p.payer.id] = 0

            for p in payOut:
                newsum = paymentData[p.receiver.id] - p.amount
                paymentData[p.receiver.id] = newsum

            for p in payIn:
                newsum = paymentData[p.payer.id] + p.amount
                paymentData[p.payer.id] = newsum
            paymentData = {k: v for k, v in paymentData.items() if k in users}
            paymentData = sum(paymentData.values())
            totalData[u] = paymentData
        return totalData


class UserDetail(APIView):
    def get(self, request, pk, format=None):
        queryset = User.objects.all()
        user = get_object_or_404(queryset, pk=pk)
        serializer = UserSerializer(user)
        return Response(serializer.data)


class UserPaymentDetail(APIView):
    def get(self, request, pk, format=None):
        print('OSJADNOASNDOAUSNDOSU')
        payOut = Transaction.objects.filter(payer=pk)  # Payer id pk
        payIn = Transaction.objects.filter(receiver=pk)  # Payer id pk
        paymentData = {}

        # initialize data
        for p in payOut:
            paymentData[p.receiver.id] = 0
            paymentData[p.receiver.id] = 0

        for p in payIn:
            paymentData[p.payer.id] = 0
            paymentData[p.payer.id] = 0

        for p in payOut:
            newsum = paymentData[p.receiver.id] - p.amount
            paymentData[p.receiver.id] = newsum

        for p in payIn:
            newsum = paymentData[p.payer.id] + p.amount
            paymentData[p.payer.id] = newsum

        contList = []

        for c in paymentData:
            da = {}
            da['id'] = c
            da['saldo'] =  paymentData[c]
            contList.append(da)

        print(contList)
        return Response(contList)


class TransactionList(generics.ListCreateAPIView):
    model = Transaction
    serializer_class = TransactionSerializer
    filter_class = TransactionFilter

    def get_queryset(self):
        params = dict(self.request.query_params.iterlists())
        queryset = Transaction.objects.all()

        if 'payer' in params.keys():
            queryset = queryset.filter(payer=params['payer'][0])
        if 'receiver' in params.keys():
            queryset = queryset.filter(receiver=params['receiver'][0])

        return queryset


class TransactionDetail(APIView):
    def get(self, request, pk, format=None):
        queryset = Transaction.objects.all()
        trans = get_object_or_404(queryset, pk=pk)
        serializer = TransactionSerializer(trans)
        return Response(serializer.data)
